Aquestes són les aplicacions i els fitxers de configuració que he hagut de editar a Debian Jessie

## prompt-tunning-development

Tunneig del prompt del terminal per a mostrar:
* Entorn virtual actiu
* Si s'està en una carpeta d'un repositori git mostra la branca activa

Manté l'usuari el host i el pwd (la ruta al directori de treball)

Per afegir-lo per defecte, una opció és afegir aquestes línies al .bashrc de l'usuari:
(suposant que el fitxer està al directori arrel de l'usuari)

```bash
if [ -f ~/prompt-tunning-development ]; then
    . ~/prompt-tunning-development
fi
```
El resultat hauria de ser quelcom semblant a això:

![prompt-tunning-development image](images/prompt-tunning-development.png)

## devilspie2

Configurar on s'obren les aplicacions d'escriptori. Cal tenir configurat el nombre d'escriptoris en estàtic, aquesta configuració utilitza 6 escriptoris.

* Ruta on es troba el fitxer: ~/.config/devilspie2/devilspie2.lua

## vimrc

Configuració de vim

* Ruta on es troba el fitxer: ~/.vimrc