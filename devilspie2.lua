function accio(workspaceNum)
    set_window_workspace(workspaceNum)
    --change_workspace(workspaceNum)
end

appName = get_application_name()

if (appName == "Icedove") then accio(1)
elseif (appName == "Iceweasel") then accio(2)
elseif (appName == "Chromium") then accio(2)
elseif (appName == "terminator") then accio(3)
elseif (appName == "nautilus") then accio(4)
elseif (appName == "Telegram") then accio(6)
elseif (appName == "Pidgin") then accio(6)
end
